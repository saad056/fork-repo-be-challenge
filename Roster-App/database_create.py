import openpyxl #for accessing excel files
import sqlite3  #for accessing Database
import csv

#################The commented code below is used for converting the csv files into xlsx files for the database creation:

# wb = openpyxl.Workbook()
# ws = wb.active
# wb2 = openpyxl.Workbook()
# ws2 = wb2.active
# with open("C:\\Slides\Biarri Coding Challenge\\be_challenge\\Roster App\\files\\shifts.csv") as f:
#     reader = csv.reader(f, delimiter=':')
#     for row in reader:
#         ws.append(row)
#
# wb.save('C:\\Slides\\Biarri Coding Challenge\\be_challenge\\Roster App\\shifts.xlsx')
#
#
# with open("C:\\Slides\Biarri Coding Challenge\\be_challenge\\Roster App\\files\\employees.csv") as t:
#     readers = csv.reader(t, delimiter=':')
#     for row in readers:
#         ws2.append(row)
#
# wb2.save('C:\\Slides\\Biarri Coding Challenge\\be_challenge\\Roster App\\employees.xlsx')


wb_Employees = openpyxl.load_workbook("C:\\Slides\Biarri Coding Challenge\\be_challenge\\Roster App\\files\\employees.xlsx") #For accessing the provided excel file (shifts)
wb_Shifts = openpyxl.load_workbook("C:\\Slides\Biarri Coding Challenge\\be_challenge\\Roster App\\files\\shifts.xlsx") #For accessing the provided excel file (employees)

ws_Employees = wb_Employees.active #for accessing the available worksheet in employees workbook
ws_Shifts = wb_Shifts.active #for accessing the available worksheet in shifts workbook

connect = sqlite3.connect("C:\Slides\Biarri Coding Challenge\\be_challenge\\Roster App\\Roster.db")  #database created/connected
cursor = connect.cursor() #handler


drop_Employees = """ DROP TABLE IF EXISTS Employees """  #If there is a table named Employees that already exists, than drop that table
drop_Shifts = """ DROP TABLE IF EXISTS Shifts """        #If there is a table named Shifts that already exists, than drop that table
EmployeesTable = """  
            CREATE TABLE IF NOT EXISTS Employees(   
            Employee_id INTEGER PRIMARY KEY,
            Firstname VARCHAR(80),
            Lastname VARCHAR(80)         
            );
         """   #creating an Employees table, with it's specified attributes
ShiftsTable = """
        CREATE TABLE IF NOT EXISTS Shifts(
        Dateofshift DATE,
        Employee_id REFERENCES Employees(Employee_id),
        Start VARCHAR(20),
        End VARCHAR(20),
        Break INTEGER
        ); 
        """   #creating a Shifts table, with it's specified attributes
#Executing queries for dropping existing tables if they exists:
cursor.execute(drop_Employees)
cursor.execute(drop_Shifts)
cursor.execute(EmployeesTable)
cursor.execute(ShiftsTable)

Employees_maxRow = ws_Employees.max_row #Maximum row number in the Employees sheet
Employees_maxCol = ws_Employees.max_column #Maximum coloumn number in the Employees sheet

Shifts_maxRow = ws_Shifts.max_row #Maximum row number in the Shifts sheet
Shifts_maxCol = ws_Shifts.max_column #Maximum coloumn number in the Shifts sheet


################################################################################################################################
########################The section below fills data in the corresponding tables:###############################################
Employees_Insertsql = """INSERT INTO Employees (Employee_id, Firstname, Lastname)
 VALUES (?,?,?);"""

Shifts_Insertsql = """INSERT INTO Shifts (Dateofshift,Employee_id, Start, End, Break)
 VALUES (?,?,?,?,?);"""


for r in range(2,Employees_maxRow):
    Employee_id = ws_Employees.cell(row=r,column=1).value
    Firstname = ws_Employees.cell(row=r,column=2).value
    Lastname = ws_Employees.cell(row=r,column=3).value
    values = (Employee_id, Firstname, Lastname)
    cursor.execute(Employees_Insertsql,values)

for r in range(2,Shifts_maxRow):
    Dateofshift = ws_Shifts.cell(row=r,column=1).value
    Employee_id = ws_Shifts.cell(row=r,column=2).value
    Start = ws_Shifts.cell(row=r,column=3).value
    End = ws_Shifts.cell(row=r,column=4).value
    Break = ws_Shifts.cell(row=r,column=5).value
    values1 = (Dateofshift,Employee_id, str(Start), str(End), Break)
    cursor.execute(Shifts_Insertsql,values1)

connect.commit() #Commiting the changes to the database
connect.close() #Closing the connection to the database
