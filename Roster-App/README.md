# Instructions

1) The database_create.py file is handling the setup, it converts the csv files into xlsx and transfers all the data to a database schema.
NOTE: You dont have to perform the setup, it has been performed for youy and a database file known as 'Roster.db' has been created. If you feel
like performing the setup again, i wont stop you, feel free to check it out.

2) You need the WebAPI.py to run the web server and after executing this file, make sure to click on the localhost link which will redirect
you to the web view page. The web view page is configured in the templates/index.html file, feel free to check that out

3) There is also a report which specifies the structure of the project, this file is known as 'Roster Management Report'. Please check that.

4) Bootstrap and jQuery have been added in the static folder

