from flask import Flask, render_template, request, redirect, url_for, flash
# from flask_mysqldb import MySQL
import sqlite3

###The following is an API to integrate MYSQL with the webserver (if required):
app = Flask(__name__)

app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'crud'

# mysql = MySQL(app)
#################################################################################

#######################The following section creates an API for the server to connect with the client and perform CRUD operations using the
#Roster.db file created from db_create.py:    -Check templates/index.html file for the webview

@app.route('/')   #this routes to home
def Index():
    # cur = mysql.connection.cursor()
    cur = sqlite3.connect("C:\\Slides\Biarri Coding Challenge\\be_challenge\Roster App\\Roster.db").cursor()
    cur.execute("SELECT  * FROM Shifts")  #Fetching all data from Shifts table and displaying it using the index.html file
    data = cur.fetchall()
    cur.close()




    return render_template('index.html', employees=data )    #Check the index.html for the Front end, employees are rendered to be displayed on the web page



@app.route('/insert', methods = ['POST'])  #This is the route to insert data on the web form
def insert():

    if request.method == "POST":     #this route uses a post request to send data in the request body
        flash("Data Inserted Successfully")
        date = request.form['date']            ####Getting information from the web form
        employee_id = request.form['employee_id']
        start = request.form['start']
        end = request.form['end']
        breaks = request.form['breaks']
        # cur = mysql.connection.cursor()                             ##############Connecting to the database and inserting data that is retrieved
        connect = sqlite3.connect("C:\\Slides\Biarri Coding Challenge\\be_challenge\Roster App\\Roster.db")
        cur = connect.cursor()
        cur.execute("INSERT INTO Shifts (Dateofshift, Employee_id, Start, End, Break) VALUES (?, ?, ? , ? , ?)", (date, employee_id, start,end,breaks))
        connect.commit()
        return redirect(url_for('Index'))   #redirecting back to home

############The following is a part of a suggested algorithm to check whether if the time assigned is between
#########start time and end time of shift previously assigned
# def in_between(now, start, end):
#     if start <= end:
#         return start <= now < end
#     else:
#         return start <= now or now < end

@app.route('/delete/<string:id_data>', methods = ['GET'])    ###Fetches the employee_id from the table in order to delete the attribute
def delete(id_data):
    flash("Record Has Been Deleted Successfully")
    connect = sqlite3.connect("C:\\Slides\\Biarri Coding Challenge\\be_challenge\Roster App\\Roster.db")
    cur = connect.cursor()
    # cur = mysql.connection.cursor()
    cur.execute("DELETE FROM Shifts WHERE Employee_id=?", (id_data,))
    # mysql.connection.commit()
    connect.commit()
    return redirect(url_for('Index'))





@app.route('/update',methods=['POST','GET'])   ####Route for updating the data using Employee_id
def update():

    if request.method == 'POST':
        id_data = request.form['id']
        date = request.form['date']
        employee_id = request.form['employee_id']
        start = request.form['start']
        end = request.form['end']
        breaks = request.form['breaks']
        # cur = mysql.connection.cursor()
        connect = sqlite3.connect("C:\\Slides\\Biarri Coding Challenge\\be_challenge\Roster App\\Roster.db")
        cur = connect.cursor()
        cur.execute("""
               UPDATE Shifts
               SET Dateofshift=?, Employee_id=?, Start=?, End=?, Break=?
               WHERE Employee_id=?
            """, (date, employee_id, start, end, breaks,id_data))
        flash("Data Updated Successfully")
        # mysql.connection.commit()
        connect.commit()
        return redirect(url_for('Index'))



if __name__ == "__main__":
    app.run(debug=True)
